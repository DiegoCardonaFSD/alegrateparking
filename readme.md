# Alégrate Parking

Nos encontramos lanzando la version 1.0 de Alégrate Parking.
Elaborado por Diego Cardona(diego0123@gmail.com).

## Generalidades

Esta plataforma se encuentra desarrollada en Laravel 5.8 con Vue.js para el front, y tomando como base Bootstrap 4.3.

Actualmente se encuentra instalado en un servidor Apache para pruebas configurado en Ubuntu, en Amazon Web Services(AWS), con base de datos MYSQL.

[Click Aquí: Alégrate](http://alegrate.thedeveloper.co)

## Datos técnicos:

- Para el consumo de servicios se realizó el uso de la librería: [GuzzleHttp](http://docs.guzzlephp.org/en/stable/)

- Para graficar las celdas, se hizo uso de la librería para Vue llamada: [vue-easeljs](https://www.npmjs.com/package/vue-easeljs)

### Desde programación, se hizo uso de:
- Migraciones.
- Seeders.
- Modelos y relaciones personalizadas.
- Archivo de rutas API.
- Ajax mediante Axios.
- La aplicación es una SPA(Single Page Application).
- Auth
- Validaciones desde el Request.
- Validaciones custom desde el Request.

## Indicaciones para usuario:

- En la pestaña Resumen sale un gráfico el cual tiene dibujadas las celdas del parqueadero y son clickeables, para la visualización de la información particular de cada celda.

- Pestaña Registrar: se utiliza para registrar vehículos nuevos cuando van llegando al parqueadero, es en este punto donde se da de alta el cliente a través del API de Alegra.

- Pestaña Gestionar: nos permite buscar un vehículo ingresando la placa o una vez se da click sobre la celda ocupada, el sistema automáticamente la ingresa y realiza la búsqueda.

- Pestaña Gestionar: después de buscar un vehículo, nos sale un botón para liberar la celda, es en este punto donde se genera la factura en Alegra a través del API.

- Pestaña Informes: cuenta con un select donde se pueden seleccionar los dos informes solicitados (Vehículos Actualmente Parqueados o 5 Celdas Más Ocupadas).

- Finalmente se creo una pestaña llamada Facturación, la idea de esta pestaña es poder ver el historial de las facturas que se han generado hasta el momento, consultada directamente con el id, desde Alegra, a través del API.

## Servicios consumidos del API de Alegra:
- Servicio de creación de contactos: https://api.alegra.com/api/v1/contacts

- Servicio de creación de facturas, incluyendo información de pago en cash: https://api.alegra.com/api/v1/invoices

- Servicio de consulta de información de compañía, para diseñar las facturas: 
https://api.alegra.com/api/v1/company

- Servicio para consultar las facturas: 
https://api.alegra.com/api/v1/invoices/:id

## Principios usados para la distribución de vehículos en las celdas:

Para la distribución de vehículos en las celdas, utilice las siguientes, reglas:

- A nivel de datos se crea tabla para celdas, con estructura matricial, es decir, haciendo uso de Filas y Columnas.

### Registrar Vehículo:
- Se genera método para asignación aleatoria entre las disponibles.
- Una vez determinada la celda asignada, procedo a enviarla al fondo en caso de que se tengan espacios.

### Eliminar Vehículo, opción Liberar Celda.
- Se valida si el vehículo tiene otros vehículos en frente.
- Se ejecuta el método de asignación aleatoria, solo para los vehículos que se encuentran en frente, excluyendo la columna donde esta el vehículo a liberar.
- Se asignan aleatoriamente de ser posible.

- De no ser posible mover los vehículos que se tienen en frente, se procede a liberar el vehículo, y ubicar los restantes en la misma columna.



## Reto Desarrollado:

Parqueadero Alégrate


Francisco desea montar un negocio de parqueadero y necesita un sistema para administrarlo.

El parqueadero cuenta con 20 celdas disponibles, éstas están distribuidos linealmente en donde se puede acomodar un vehículo delante de otro vehículo, como se puede observar en la siguiente imagen: 




Francisco necesita una sistema que permita registrar el ingreso de un vehículo y le asigne una celda aleatoria.  


Cuando se registre la salida del vehículo se debe mostrar el valor a pagar contando los segundos que el vehículo estuvo ocupando la celda (el valor del parqueo es de 30 pesos por segundo), adicionalmente se debe registrar la venta creando una factura en Alegra con este valor.


¿Cómo funciona?

Francisco necesita registrar el ingreso de un vehículo, indicando la marca del vehículo y la placa/licence plate de éste.

El sistema debe asignar al vehículo una celda aleatoria, tener en cuenta que si la celda de “atrás” se encuentra disponible, ésta debe ser la que se asigne al vehículo.

Francisco debe poder registrar la salida de un vehículo, en donde se debe ingresar la placa/licence plate del vehículo que desea salir. Es posible que el vehículo que desea salir tenga otro obstruyéndole la salida, en este caso se debe asignar al vehículo que está obstruyendo otra celda, si hay disponible, si no, se debe asignar la celda del vehículo que desea salir. 

Cuando el vehículo salga del parqueadero debe generarse la factura por el parqueo indicando los segundos que se ocupó la celda y el total a pagar (cada segundo cuesta 30 unidades), la factura se debe generar en Alegra


Francisco necesita observar en el sistema:

La distribución de las celdas, mostrando las celdas disponibles y las ocupadas.

Reporte de los vehículos que se encuentran actualmente en el parqueadero, mostrando la placa, la celda original a la cual ingresó el vehículo, la celda final y el tiempo transcurrido en el parqueadero

Se necesita un reporte que indique las 5 celdas más ocupadas del parqueadero y los segundos que han sido ocupadas.
