<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'v1'], function () {
	Route::get('cells', 'CellController@index');
	
	Route::get('brands', 'BrandController@index');
	Route::post('record/create', 'RecordController@store');
	Route::get('record/{id}', 'RecordController@show');
	Route::post('record/{id}', 'RecordController@destroy');
	Route::get('report/occupied', 'ReportController@getCurrentRecords');
	Route::get('report/five_more_occupied', 'ReportController@getFiveMoreOcuppiedCells');
	Route::get('report/company', 'ReportController@getCompany');
	Route::get('report/invoice/{id}', 'ReportController@getInvoice');
	Route::get('report/invoices', 'ReportController@getReportInvoices');
	
});