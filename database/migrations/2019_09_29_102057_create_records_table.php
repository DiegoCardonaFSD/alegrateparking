<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('license_plate', 6);
            $table->integer('brand_id')->unsigned();
            $table->dateTime('in_at');
            $table->dateTime('out_at')->nullable();
            $table->integer('cell_in_id')->unsigned()->nullable();
            $table->integer('cell_out_id')->unsigned()->nullable();
            $table->integer('alegra_client_id')->unsigned()->nullable();
            $table->integer('alegra_invoice_id')->unsigned()->nullable();
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('alegra_client_id')->references('id')->on('alegra_clients');
            $table->foreign('cell_in_id')->references('id')->on('cells');
            $table->foreign('cell_out_id')->references('id')->on('cells');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
