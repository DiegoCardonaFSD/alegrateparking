<?php

use Illuminate\Database\Seeder;

class CellTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cells')->insert([
        	 ['col' => 1, 'row' => 1]
        	,['col' => 2, 'row' => 1]
        	,['col' => 3, 'row' => 1]
        	,['col' => 4, 'row' => 1]
        	,['col' => 5, 'row' => 1]
        	,['col' => 1, 'row' => 2]
            ,['col' => 2, 'row' => 2]
            ,['col' => 3, 'row' => 2]
            ,['col' => 4, 'row' => 2]
            ,['col' => 5, 'row' => 2]
            ,['col' => 1, 'row' => 3]
            ,['col' => 2, 'row' => 3]
            ,['col' => 3, 'row' => 3]
            ,['col' => 4, 'row' => 3]
            ,['col' => 5, 'row' => 3]
            ,['col' => 1, 'row' => 4]
            ,['col' => 2, 'row' => 4]
            ,['col' => 3, 'row' => 4]
            ,['col' => 4, 'row' => 4]
            ,['col' => 5, 'row' => 4]
        	]);
    }
}
