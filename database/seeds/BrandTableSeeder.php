<?php

use Illuminate\Database\Seeder;

class BrandTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
        	 ['name' => 'Audi']
        	,['name' => 'Mazda']
        	,['name' => 'Jeep']
        	,['name' => 'Fiat']
        	,['name' => 'Toyota']
        	,['name' => 'Renault']
            ,['name' => 'Chevrolet']
            ,['name' => 'BMW']
            ,['name' => 'LandRover']
            ,['name' => 'Volkswagen']
            ,['name' => 'Acura']
            ,['name' => 'Jaguar']
        	]);
    }
}
