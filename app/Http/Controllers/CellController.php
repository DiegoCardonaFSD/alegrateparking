<?php

namespace App\Http\Controllers;

use App\Cell;

use Illuminate\Http\Request;

class CellController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $data = Cell::all();
        $data = Cell::all();
        $data->each(function($data){
          $data->records_active;
        });
        return response()->json([
            "status" => true,
            "data" => $data
          ]); 


    }

}
