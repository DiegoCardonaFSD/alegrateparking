<?php

namespace App\Http\Controllers;

use App\Record;
use App\Cell;
use DateTime;
use App\Http\Requests\RecordRequest;
use App\Http\Controllers\AlegraApiController;

use Illuminate\Http\Request;

class RecordController extends Controller
{
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    public function store(RecordRequest $request)
    {
        $validated = $request->validated();

        $license_plate = $request->input('license_plate');
        $brand_id = $request->input('brand');
        $dt = new DateTime;

        //creamos el cliente en alegra
        $contact = AlegraApiController::getContact($license_plate);
        if(!$contact){
            return response()->json([
                "status" => false,
                "message" => 'Ha ocurrido un problema intentando crear el contacto'
              ]);
        }


        $randomCell = $this->getRandomCell();
        if($randomCell == 'no_cells'){
            return response()->json([
                "status" => false,
                "message" => 'No hay celdas disponibles'
              ]);
        }

        $record = new Record();
        $record->license_plate = $license_plate;
        $record->brand_id = $brand_id;
        $record->in_at = $dt->format('y-m-d H:i:s');
        $record->cell_in_id = $randomCell->id;
        $record->cell_out_id = $randomCell->id;
        $record->cell_out_id = $randomCell->id;
        $record->alegra_client_id = $contact->id;
        if($record->save()){

            $randomCell->empty = false;
            $randomCell->update();

            return response()->json([
                "status" => true,
                "message" => 'Vehiculo registrado con exito'
              ]);
        }else{
            return response()->json([
                "status" => false,
                "message" => 'Ha ocurrido un problema registrando el vehiculo'
              ]);
        }

    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $records = Record::with(['cell','cell_in'])->where([
                    ['license_plate','=',$id],
                ])->whereNull('out_at')->get();

        if(count($records) > 0){
            return response()->json([
                "status" => true,
                "data" => $records[0]
              ]);
        }

        return response()->json([
                "status" => false,
                "message" => 'El vehiculo no ha sido encontrado'
              ]);
        
    }

   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = Record::with(['cell','cell_in', 'client'])->where([
                    ['id','=',$id],
                ])->whereNull('out_at')->first();
        
        if($record != null){
            $cell = $record->cell;
            $response = $this->processCellsBeforeDelete($cell);

            //non_in_front: procedemos a sacar el vehiculo
            //pending_to_move : procedemos a sacar el vehiculo y luego enviamos al fondo
            //processed: procedemos a sacar el vehiculo

            // if($record->delete()){
            $cell->empty = 1;
            $cell->update();
            $record->out_at = date("Y-m-d H:i:s");
            $record->update();

            //generando factura

            $invoice = AlegraApiController::createInvoiceAndPayment($record, $record->client, $this->getDifferenceInSeconds($record->in_at, $record->out_at));

            $record->alegra_invoice_id = $invoice['id'];
            $record->update();
            
            if($response == 'pending_to_move'){
                $this->processCellsAfterDelete($cell);
            }

             return response()->json([
                "status" => true,
                "message" => 'El vehiculo ha sido liberado con exito',
                "invoice" => $invoice
              ]);
            // }           
        }

        return response()->json([
                "status" => false,
                "message" => 'El vehiculo no ha sido encontrado'
              ]);
    }

    
    public function getDifferenceInSeconds($date1, $date2){
        $timeFirst  = strtotime($date1);
        $timeSecond = strtotime($date2);
        return $timeSecond - $timeFirst;
    }


    /***************************************/

    public function sendToBack($cell){
        $cells = Cell::where([
                                ['empty', '=', '1']
                                ,['col', '=', $cell->col]
                                ,['row', '<', $cell->row]
                            ])->orderBy('row', 'ASC')->get();
        if(count($cells) > 0){
            return $cells[0];
        }
        return $cell;
    }

    public function getRandomCell($row = 0, $col = 0){
        //lo primero que hacemos es obtener una celda disponible aleatoria
        $where = [];
        //ignoramos las celdas que hay en esta columna
        if($row != 0 && $col != 0){
           $where[] = ['col', '!=', $col];
        }
        $where[] = ['empty', '=', '1'];
        $cell = Cell::where($where)->inRandomOrder()->first();
        if($cell === null){
            return 'no_cells';
        }

        //ahora lo que debemos hacer, es respetar la regla obligando a que la celda a devolver sea la última.
        return $this->sendToBack($cell);
    }

    /********************/

    public function getCellsInFront($row, $col){
        return Cell::with('records_active')->where([
                                ['empty', '=', '0']
                                ,['col', '=', $col]
                                ,['row', '>', $row]
                            ])->orderBy('row', 'ASC')->orderBy('col','DESC')->get();
    }

    public function moveCellBeforeDelete($cell){
        $cellToMove = $this->getRandomCell($cell->row, $cell->col);
        if($cellToMove == 'no_cells'){
            return false;
        }
        
        if(count($cell->records_active) == 0){
            return false;
        }
        //movemos el registro
        $cell->records_active[0]->cell_out_id = $cellToMove->id;
        $cell->records_active[0]->update();
        //actualizamos la celda nueva
        $cellToMove->empty = false;
        $cellToMove->update();
        //actualizamos la celda anterior
        $cell->empty = true;
        $cell->update();
        return true;
    }

    public function processCellsBeforeDelete($cell){
        //obtenemos las celdas que tiene en frente
        $cells = $this->getCellsInFront($cell->row, $cell->col);

        //si no tenemos celdas en frente, daamos el aval para eliminar
        if(count($cells) == 0){
            return 'non_in_front';
        }

        //si si tenemos celdas en frente, movemos una a una
        foreach ($cells as $current_cell) {
            $response = $this->moveCellBeforeDelete($current_cell);
            if(!$response){
                return 'pending_to_move';
            }
        }
        return 'processed';
    }

    public function processCellsAfterDelete($cell){
        $cells = $this->getCellsInFront($cell->row, $cell->col);
        //si no tenemos celdas en frente, daamos el aval para eliminar
        if(count($cells) == 0){
            return 'non_in_front';
        } 
        // procedemos a enviar al fondo celda por celda
        foreach ($cells as $current_cell) {
            $cellToMove = $this->getRandomCell();
            
            if($cellToMove == 'no_cells'){
                return false;
            }
            $current_cell->records_active[0]->cell_out_id = $cellToMove->id;
            $current_cell->records_active[0]->update();
            //actualizamos la celda nueva
            $cellToMove->empty = false;
            $cellToMove->update();
            //actualizamos la celda anterior
            $current_cell->empty = true;
            $current_cell->update();

        }

    }
}
