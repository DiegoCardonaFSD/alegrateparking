<?php

namespace App\Http\Controllers;

use App\Record;
use App\Cell;

use Illuminate\Http\Request;
use App\Http\Controllers\AlegraApiController;

class ReportController extends Controller
{
    public function getCurrentRecords(){
        $data = Record::with(['cell','cell_in'])->whereNull('out_at')->get();
        return response()->json([
            "status" => true,
            "data" => $data,
            "current_date" => date("Y-m-d H:i:s"),
          ]);
    }

    public function getFiveMoreOcuppiedCells(){

        $data = Record::with('cell')->groupBy('cell_out_id')
                        ->selectRaw('cell_out_id, SUM(TIMESTAMPDIFF(MICROSECOND,in_at,IFNULL(out_at, CURRENT_TIMESTAMP()) )) total')
                        ->orderBy('total', 'DESC')
                        ->limit(5)
                        ->get();
        $mysql = Record::selectRaw('CURRENT_TIMESTAMP()')->get();
        return response()->json([
            "status" => true,
            "data" => $data,
            "current_date" => date("Y-m-d H:i:s"),
            "current_date_mysql" => $mysql,
          ]);
    }

    public function getCompany(){
        $company = AlegraApiController::getCompany();
         return response()->json([
            "status" => true,
            "data" => $company
          ]);
    }
    public function getInvoice($id){
        $invoice = AlegraApiController::getInvoice($id);
         return response()->json([
            "status" => true,
            "data" => $invoice
          ]);
    }

    public function getReportInvoices(){
        $data = Record::with(['cell','cell_in'])->whereNotNull('alegra_invoice_id')->get();
        return response()->json([
            "status" => true,
            "data" => $data,
          ]);
    }
}
