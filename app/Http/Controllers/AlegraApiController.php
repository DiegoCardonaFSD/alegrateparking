<?php

namespace App\Http\Controllers;

use App\AlegraClient;
use Illuminate\Http\Request;

class AlegraApiController extends Controller
{   

    public static function getHeaders(){
         return [
            'Authorization' => 'Basic ' . env("ALEGRA_TOKEN",''),        
            'Accept'        => 'application/json',
            'Content-Type'  => 'application/json',
        ];
    }

    public static function getContact($license_plate){
        $contact = AlegraClient::where('license_plate', '=', $license_plate)->first();
        if($contact === null){
            $contact = self::createContact($license_plate);
            if(!$contact){
                return $contact;
            }
            return self::saveContact($contact);
        }
        return $contact;
    }

    public static function saveContact($contact){
        $alegraClient = new AlegraClient();
        $alegraClient->license_plate    = $contact['identification'];
        $alegraClient->alegra_client_id = $contact['id'];
        if($alegraClient->save()){
            return $alegraClient;
        }
        return false;
    }

    public static function createContact($license_plate){

        $headers = self::getHeaders();

        $client = new \GuzzleHttp\Client();
        $url = "https://api.alegra.com/api/v1/contacts";

        $data['name']           = "vehiculo $license_plate";
        $data['identification'] = $license_plate;

        $response = $client->request('POST', $url, ['json'=>$data, 'headers' => $headers]);
        // $response = $request->send();
        if($response->getStatusCode() == 201){
            $data = $response->getBody()->getContents();
            $data = json_decode($data, true);
            return $data;
        }
        return false;
    }

    public static function createInvoiceAndPayment($record, $contact, $time = 0){

        $headers = self::getHeaders();

        $client = new \GuzzleHttp\Client();
        $url = "https://api.alegra.com/api/v1/invoices";

        $data['date']    = date("Y-m-d");
        $data['dueDate'] = date("Y-m-d");
        $data['client']  = $contact->alegra_client_id;

        $item['id'] = 1;
        $item['description'] = "Parqueo $time segundos vehiculo $record->license_plate";
        $item['price'] = env("ALEGRA_PRECIO_X_SEGUNDO",'');
        $item['quantity'] = $time;
        $data['items'] = array($item);

        $payment['date'] = date("Y-m-d");
        $payment['account'] = array("id" => 2);
        $payment['amount'] = intval($time)*intval($item['price']);
        $payment['currency'] = array("code" => "COP", "exchangeRate" => 1);
        $data['payments'] = array($payment);


        $response = $client->request('POST', $url, ['json'=>$data, 'headers' => $headers]);
        // $response = $request->send();
        if($response->getStatusCode() == 201){
            $data = $response->getBody()->getContents();
            $data = json_decode($data, true);
            return $data;
        }
        return false;

    }

    public static function getCompany(){
        $headers = self::getHeaders();

        $client = new \GuzzleHttp\Client();
        $url = "https://api.alegra.com/api/v1/company";

        $response = $client->request('GET', $url, ['headers' => $headers]);
        // $response = $request->send();
        //dd($response);
        if($response->getStatusCode() == 200){
            $data = $response->getBody()->getContents();
            $data = json_decode($data, true);
            return $data;
        }
        return false;
    }

    public static function getInvoice($id){
        $headers = self::getHeaders();

        $client = new \GuzzleHttp\Client();
        $url = "https://api.alegra.com/api/v1/invoices/$id";
        

        $response = $client->request('GET', $url, ['headers' => $headers]);
        // $response = $request->send();
        if($response->getStatusCode() == 200){
            $data = $response->getBody()->getContents();
            $data = json_decode($data, true);
            return $data;
        }
        return false;

    }
}
