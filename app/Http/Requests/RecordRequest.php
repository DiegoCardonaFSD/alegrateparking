<?php

namespace App\Http\Requests;

use App\Record;

use Illuminate\Foundation\Http\FormRequest;

class RecordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        \Validator::extend( 'parked_license_plate', function ( $attribute, $value, $parameters, $validator ) {
                $fields = [ $attribute => $value ];
                

                $records = Record::where([
                                ['license_plate', '=', $value]
                            ])->whereNull('out_at')->get();
                if(count($records) > 0){
                    return false;
                }
                return true;
            }, 'El vehiculo se encuentra actualmente aparcado en nuestras instalaciones' );


        return [
            'license_plate' => 'required|parked_license_plate',
            'brand' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'license_plate.required' => 'Debe ingresar una placa',
            'brand.required'  => 'Debe seleccionar una marca',
        ];
    }
}
