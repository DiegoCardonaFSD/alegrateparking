<?php

namespace App;

use App\Cell;
use App\AlegraClient;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    //
    public function cell()
    {
        return $this->hasOne('\App\Cell', 'id', 'cell_out_id');
    }

    public function cell_in()
    {
        return $this->hasOne('\App\Cell', 'id', 'cell_in_id');
    }

     public function client()
    {
        return $this->hasOne('\App\AlegraClient', 'id', 'alegra_client_id');
    }
}
