<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cell extends Model
{
    public function records()
    {
       return $this->hasMany('App\Records');
    }

    public function records_active()
    {
       return $this->hasMany('App\Record','cell_out_id','id')->whereNull('out_at');
    }


}
