
require('./bootstrap');

window.Vue = require('vue');
window.Vue.use(require('vue-easeljs'));

import Vue from 'vue'
import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'
 
window.toastr = require('toastr')
window.toastr.options.closeMethod = 'fadeOut';
window.toastr.options.closeDuration = 300;
window.toastr.options.closeEasing = 'swing';
 
Vue.use(VueToastr2)

Vue.component('dashboard', require('./components/DashboardComponent.vue').default);
Vue.component('resume', require('./components/ResumeComponent.vue').default);

Vue.component('car-manager', require('./components/CarManagerComponent.vue').default);
Vue.component('car-manager-header', require('./components/CarManagerHeaderComponent.vue').default);
Vue.component('car-manager-new', require('./components/NewCarComponent.vue').default);
Vue.component('car-manager-view', require('./components/ViewCarComponent.vue').default);

Vue.component('car-manager-report', require('./components/ReportComponent.vue').default);
Vue.component('car-manager-report-current-vehicles', require('./components/ReportCurrentVehiclesComponent.vue').default);
Vue.component('car-manager-report-five-cells', require('./components/ReportFiveCellsComponent.vue').default);

Vue.component('car-manager-report-invoices', require('./components/ReportInvoicesComponent.vue').default);
Vue.component('car-manager-report-invoice', require('./components/ReportInvoiceComponent.vue').default);
Vue.component('car-manager-invoice', require('./components/InvoiceComponent.vue').default);


const app = new Vue({
    el: '#app',
});
